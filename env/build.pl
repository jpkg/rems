#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

use File::Spec::Functions qw(catfile rel2abs);
use File::Basename;

my $srcdir = rel2abs (dirname (__FILE__));
my $dir = dirname $srcdir;
my $envpath = catfile ($srcdir, "env.sh");

my $fh;
if (! -f $envpath) {
    open ($fh, ">", $envpath) or die "unable to open.";

    print $fh <<EOF;
export PATH="$dir/src:\$PATH"
export MT_EDITOR=vi
EOF
}

my $SHELL = $ENV{'SHELL'};
my $config = "";

if ($SHELL =~ /\/zsh$/) {$config = catfile $ENV{'HOME'},".zshrc";}
elsif ($SHELL =~ /\/bash$/) {$config = catfile $ENV{'HOME'},".bashrc";}
elsif ($SHELL =~ /\/csh$/) {$config = catfile $ENV{'HOME'},".cshrc";}

else {
    print "Your shell $SHELL is not supported. You can manually source the env.sh file in your configuration.";
    exit 0;
}

open($fh, ">>", $config) or die "unable to open shell configuration $config";

print "Do you want the script to source the\nenv.sh file at $config?[Y/n]";
my $ans = <STDIN>;
chomp $ans;
if ($ans eq "Y") {
    print $fh "source \"$envpath\"\n";
}


# Todo Program

This is partially inspired by [Lincoln Auster's
disorg](https://github.com/LincolnAuster/disorg), but aims to add additional
functionality.

I tend to write todos, but never check them, so the program should be able to
display reminders so that I always see them (aka show them on shell startup).

# Functionality

- Parse a folder with files that contain one reminder each.
- Be able to create and remove reminders in the shell.
- Cleanup one-time reminders.
- Show reminders based on levels of urgency.
- Be able to set when the reminders become urgent.

## States
- 3 levels (nonurgent, prepare, urgent)
- These levels can be set or never set.
- If prepare date is after urgent date, prepare just never happens.

## Additional Functionality (May not be used if difficult)
### Weekly reminders
- urgent and prepare dates cannot be more than 7 days before
- The reminders are not removed by a cleanup.

### Other Functionality
- Autocompletion functionality in the shell for when one tries to remove reminders.

# Additional Notes/Tools to use

- Either use a command line argument or an environmental variable (default) to
determine what directory to search files for.

- Dmenu for removing/searching reminders (or just a basic 1-n selection menu)
    for if no arguments are given.
- Use the user's `$EDITOR` variable or just `vi` to edit reminders.

## Warnings

- If they try to edit/remove a reminder which is in its critical zone.
- Alert the user if the reminder that they are trying to remove does not exist
(add -rf flag to supress this).
- If they try to edit multiple reminders at the same time. (Only edit the first
one).
- All `-h` arguments should cause the help output to be printed
- Unknown commands should do the same.

## Stack to use



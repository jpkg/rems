import datetime
import os
import sys
import subprocess
import editor.parse as p
from state import *
import state.format as c

editor = lambda: os.getenv("REM_EDITOR") or "vi"
get_path = lambda d, n: os.path.join(d, n + ".yml")

def edit(directory, name, statustype=Status):
    path = get_path(directory, name)
    if not os.path.isfile(path):
        print(f'New reminder {name} created')
        with open(path, 'w') as w:
            w.write(p.default_yml(name, statustype))

    f_prev = open(path).read()
    subprocess.call([editor(), path])
    f = open(path).read()
    try:
        p.parse_state(f)
    except Exception:
        sys.stderr.write(c.color("red", "Error:") + " parse error: likely wrong weekday formatting or datetime error?")
        with open(path, 'w') as w:
            w.write(f_prev)
    return p.parse_state(f).name, f

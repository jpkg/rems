import editor.edit as e
import editor.parse as p
from state import *
import os
import sys
import subprocess

filestate = lambda f : p.parse_state(open(f, "r").read())
get_path = lambda d, n: os.path.join(d, n + ".yml")

# deleting multiple reminders.
def delete(directory,names:list[str], warn=True):
    for name in names:
        path = get_path(directory, name)
        _delete_warning(path, warn)

    for name in names:
        path = get_path(directory, name)
        if os.path.isfile(path):
            os.remove(path)
            sys.stderr.write(f'Removing reminder {name}\n')
    
def _delete_warning(path, warn=True):
    if not warn:
        return
    if not os.path.isfile(path):
        sys.stderr.write(f'{path} is a non-existent reminder.\n')
        sys.exit(1)
    s = filestate(path)
    if s.status.status in [2, 3] or type(s.status) != Status:
        x = input(f'''Reminder "{s.name}" is of an urgent status or is cyclic.
Are you sure you want to remove it? [Y/n] ''')
        if x != 'Y':
            sys.stderr.write(f"Reminder {s.name} removal aborted.\n")
            sys.exit(1)


# removing passed reminders.
def cleanup(directory):
    rems = [filestate(os.path.join(directory, f)) for f in os.listdir(directory) if f[-3:] == "yml"]
    passed = [f.name for f in rems if f.status.status == 3]
    delete(directory, passed,warn=False)


# listing all reminders.
def listrems(directory, show=[2, 1, 0], minimal=False):
    rems = [filestate(os.path.join(directory, f)) for f in os.listdir(directory)
    if f[-3:] == "yml"]
    rems = [[x for x in rems if x.status.status == i] for i in range(4)]
    for i in show:
        for x in rems[i]:
            if minimal:
                print(x.name)
            else:
                print(x)

def info(directory, name):
    path = get_path(directory, name)
    if not os.path.isfile(path):
        sys.stderr.write(f'{path} is a non-existent reminder.\n')
        sys.exit(1)
    print(filestate(path).notes.view())

def edit(directory, name, statustype):
    path = get_path(directory, name)
    n, f = e.edit(directory, name, statustype)
    if n != name:
        path = get_path(directory, n)
        _exists_warning(path)
        os.remove(get_path(directory,name))
        open(path, "w").write(f)


def _exists_warning(path):
    if os.path.isfile(path):
        x = input(f'''The reminder {path} is already present.
Are you sure you want to overwrite it? [Y/n] ''')
        if x != 'Y':
            sys.stderr.write("Process Aborted.\n")
            sys.exit(1)

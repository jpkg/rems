import datetime
from state import *
import yaml
import datetime

# returns a default file.
def default_yml(name, statustype=Status):
    return yaml.dump(State.default(name, statustype), sort_keys=False, indent=4, line_break=3)

def parse_state(s:str) -> State:
    d = yaml.safe_load(s)
    try:
        return State.parse(d)
    except Exception:
        raise Exception

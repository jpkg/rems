from state.notes import Notes
from state.status import Status
from state.weekly import Weekly

remtypes = {
    'weekly': Weekly,
    'default': Status,
}

class State:

    def __init__(self, name:str, st, nt:Notes):
        self.name = name
        self.notes = nt
        self.status = st

    def __str__(self):
        return f'Name: {self.name}\n{self.status}\n{self.notes}\n'

    def view(self):
        return self.notes.view()

    @classmethod
    def default(cls, name:str, statustype=Status):
        return {
            'name': name,
            'status': statustype.default(),
            'notes': Notes.default()
        }
    
    @classmethod
    def parse(cls, d:dict):
        statustype = remtypes[d['status']['type']]
        return State(d['name'], statustype.parse(d['status']), Notes.parse(d['notes']))

from state.status import Status
import state.format as c
import datetime

_days = [
    ('monday', 'mon'),
    ('tuesday', 'tue', 'tues'),
    ('wednesday', 'wed'),
    ('thursday', 'thu', 'thurs'),
    ('friday', 'fri'),
    ('saturday', 'sat'),
    ('sunday', 'sun'),
]

def _day_of_week(s:str) -> int:
    for d in range(7):
        if s.lower() in _days[d]:
            return d
    raise KeyError

class Weekly(Status):
    def __init__(self, dates, order, completed=False):
        super().__init__(dates, order, completed)

    def __str__(self):
        return f'Type: {c.color("blue", "Weekly")}\nStatus: {Status._messages[self.status]}'

    @classmethod
    def default(cls):
        d = datetime.datetime.now()
        rd = [
            ('dayofweek', _days[d.weekday()][0].capitalize()), 
            ('hour', 0), 
            ('minute', 0)
        ]
        res = {
            'nonurgent': dict([('active', False)] + rd),
            'prepare': dict([('active', False)] + rd),
            'critical': dict([('active', False)] + rd),
            'passed': dict([('active', False)] + rd),
            'type': 'weekly',
        }
        return res

    @classmethod
    def parse(cls, d:dict):
        dates = []
        order = []
        now = datetime.datetime.now()
        completed = False
        for k,ind in zip(['nonurgent', 'prepare', 'critical', 'passed'], range(4)):
            if d[k]['active']:
                order.append(ind)
                try:
                    day = now.date() - datetime.timedelta(
                        days=((-_day_of_week(d[k]['dayofweek']) + now.weekday() + 7)%7)
                    )
                except KeyError:
                    raise KeyError
                day = datetime.datetime(
                    day.year,
                    day.month,
                    day.day,
                    d[k]['hour'],
                    d[k]['minute'],
                )
                if day > now:
                    day -= datetime.timedelta(days=7)
                dates.append(day)
        if 'completed' in d:
            completed = d['completed']
        return Weekly(dates, order)

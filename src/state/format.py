_colors = {
    'white': '\033[1;40m',
    'red': '\033[1;31m',
    'green': '\033[1;32m',
    'yellow': '\033[1;33m',
    'blue': '\033[1;34m',
    'purple': '\033[1;35m'
}

_reset = '\033[0m'

color = lambda color, s : _colors[color] + str(s) + _reset
underline = lambda s : '\033[4m' + str(s) + _reset

import datetime
import state.format as c

class Status:
    _messages = [
        c.color('green', 'Non-urgent'),
        c.color('yellow', 'Prepare'),
        c.color('red', 'critical'),
        c.color('white', 'passed')
    ]

    def __init__(self, dates, order, completed=False):
        if not all(type(x) == datetime.datetime for x in dates):
            raise ValueError("ReminderDate constructor failed: not all arguments are datetime")
        self.completed = completed
        self.times = list(zip(dates, order))
        self.times.sort()
        self.evalstatus()
   
    def evalstatus(self):
        if self.completed:
            self.status = 3
            return
        self.status = 0
        now = datetime.datetime.now()
        for t in self.times:
            if t[0] < now:
                self.status = t[1]

    def __str__(self):
        return 'Type: {}\nStatus: {}'.format(c.color("blue", "Default"), Status._messages[self.status])
    @classmethod
    def default(cls):
        d = datetime.datetime.now()
        rd = {
            'active': False,
            'year': d.year,
            'month': d.month,
            'day': d.day,
            'hour': 0,
            'minute': 0,
        }
        res = { 
            'prepare': rd.copy(), 
            'critical': rd.copy(), 
            'passed': rd.copy(),
            'type': 'default',
            'completed': False
        }
        return res

    @classmethod
    def parse(cls, d:dict):
        dates = []
        order = []
        completed = False
        for k,ind in zip(['prepare', 'critical', 'passed'], range(1, 4)):
            if d[k]['active']:
                order.append(ind)
                dates.append(datetime.datetime(
                    d[k]['year'],
                    d[k]['month'],
                    d[k]['day'],
                    d[k]['hour'],
                    d[k]['minute'],
                ))
            if 'completed' in d:
                completed = d['completed']
        return Status(dates, order, completed=completed)

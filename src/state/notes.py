import subprocess
import state.format as c

class Notes:

    def __init__(self, brief, extended):
        self.brief = brief
        self.extended = extended

    def view(self):
        return '{}\n{}\n\n{}\n{}'.format(c.underline("Brief"), self.brief, c.underline("Extended"), self.extended)

    def __str__(self):
        return '{}\n{}'.format(c.color('blue', 'Notes:'), self.brief)

    @classmethod
    def parse(cls, d:dict):
        return Notes(d['briefnotes'], d['longnotes'])

    @classmethod
    def default(cls):
        return { 
            'briefnotes': "",
            'longnotes': "",
        }

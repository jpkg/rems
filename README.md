# Rems

This is partially inspired by [Lincoln Auster's
disorg](https://github.com/LincolnAuster/disorg), but aims to add additional
functionality.

# Installation

Clone this repository, then run the `/build.sh` script. This will create a
config.sh file that contains all necessary environmental variables. It will
also append the source statement to the end of your shell configuration file.

# Dependencies

- pyyaml

# On the reminder files

All reminders are represented by a yml file located in the `REMS_DIR` environmental
variable that can be edited in `config.sh`.

## State of the Reminder

There are 4 states that a reminder can be in (and these must come sequentially)

1. Non-urgent (Something to keep at the back of your head)

2. Prepare (Begin preparations for this, but no need to rush)

3. Critical (You absolutely must do this, sufficient for the panic monster to notice)

4. Passed (The event has passed, and is marked for cleanup to prevent clutter)

# Commands

## Editing/Adding reminders

`rems edit [ --weekly | --default ] [name]`

If the reminder name does not exist, a new reminder will be generated. If the
--weekly flag is used on a reminder that is default or vice versa, the flag
will have no effect.

## Removing reminders

`rems remove [-rf] [files to remove ...]`

The -rf flag will remove all warnings about the status of the reminder or
whether it exists or not.

## Listing all reminders

`rems list [--show [SHOW]] [--minimal]`

If you decide to use the --show flag, the string after that indicates in what
way you want to display the reminders. The following characters and their
corresponding meanings are below:

- n - non urgent
- r - prepare
- c - critical
- p - passed

For example, calling `rems list --show nrcp` will print all of the non-urgent
reminders, then the reminders in the prepare state, then those in the critical
state, and finally the reminders that have passed.

The default (if you do not invoke --show) is `crn`.

The --minimal flag outputs plain output listing the names of all reminders (useful for scripts)

# Getting Extended Notes about a reminder

`rems notes [files...]`

# Removing all reminders marked as passed

`rems cleanup`
